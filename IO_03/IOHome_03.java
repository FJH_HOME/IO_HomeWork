package IOHome;
/*
 * 码云作业三：
使用DataInputStream和DataOutputStream实现从控制台中读入一个文件名，
判断该文件是否存在。
如果该文件存在，则在原文件相同
路径下创建一个文件名为“copy_原文件名”的新文件，该文件内容为原文件的拷贝。
例如：读入
D:/img/photo.jpg
则创建一个文件
D:/img/copy_photo.jpg
新文件内容和原文件内容相同
 */
import java.io.*;
public class IOHome_03 {

	public static void main(String[] args) {
		try
		{
			File file=new File("E:\\photo.txt");
			if(file.exists())
			{
				String name=file.getName();
				File address=file.getParentFile();
				File file2=new File(address+"copy_"+file.getName());
				file2.createNewFile();
				DataInputStream dis=new DataInputStream(new FileInputStream(file));
				DataOutputStream dos=new DataOutputStream(new FileOutputStream(file2));
				int ch;
				while((ch=dis.read())!=-1)
				{
					dos.write(ch);
				}
				System.out.println("文件复制成功！！！");
				dis.close();
				dos.close();
			}else
			{
				System.out.println("文件不存在");
			}
			
			
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
