package IOHome;
/*
 * 码云作业五：
指定一个文件夹，然后列出文件夹下面的所有子文件与文件夹，但是格式要如下：

文件：
        文件名1
        文件名2
          ……
文件夹：
        文件夹名1
        文件夹名2
        文件夹名3
          ……
 */
import java.io.*;
import java.util.Scanner;
public class IOHome_05 {
	
	public static void SearchFile(File file)
	{
		if(!file.exists())
		{
			System.out.println("文件或文件夹不存在！！！");
		}else
		{
		if(file.isFile())
		{
			System.out.println("只有一个文件"+file.getName());
		}else if(file.isDirectory())
		{
			File[] f=file.listFiles();
			System.out.println("文件:");
			for(File fi:f)
			{
				if(fi.isFile())
				{
					System.out.println("\t"+fi.getName());
				}
			}
			System.out.println("文件夹:");
			for(File fi:f)
			{
				if(fi.isDirectory())
				{
					System.out.println("\t"+fi.getName());
				}
			}
		}
		}
	}

	public static void main(String[] args) {
	    
		Scanner input=new Scanner(System.in);
		System.out.println("请输入您要查询的文件夹或文件：");
		String path=input.nextLine();
		File file=new File(path);
		SearchFile(file);

	}

}
