package IOHome;
/*
 * （FileInputStream 和FileOutputStream）利用FileInputStream 和FileOutputStream，完成
下面的要求：
1） 用FileOutputStream 在当前项目doc目录下创建一个文件“test.txt”，并向文件写入“Hello
World”，如果文件已存在，则在原有文件内容后面追加内容。
2） 用FileInputStream 读取test.txt 文件，并在控制台上打印出test.txt 中的内容。
3） 要求用try-catch-finally 处理异常，并且关闭流应放在finally 块中。
 */
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.BufferedWriter;
import java.io.File;

public class IOHome_01 {

	public static void main(String[] args) {
		try {
		FileOutputStream fos=new FileOutputStream("N\\test.txt",true);
		String info="Hello World";
		fos.write(info.getBytes());
		fos.close();
		FileInputStream fis=new FileInputStream("N\\test.txt");
		byte a;
		while((a=(byte) fis.read())!=-1)
		{
			System.out.print((char)a);
		}
		fis.close();
		}catch(IOException e)
		{
			e.printStackTrace();
		}

	}

}
