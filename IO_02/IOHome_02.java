package IOHome;
/*
 * 码云作业二：
（使用BufferedReader类和FileReader类）完成下面操作。
在当前目录下创建一个worldcup.txt 的文本文件，其格式如下：
1998/法国
2002/巴西
2006意大利
2010/西班牙
2014/德国…
该文件采用“年份/世界杯冠军”的方式保存每一年世界杯冠军的信息。
要求：读入该文件的基础上，让用户输入一个年份，输出该年的世界杯冠军。如果该年
没有举办世界杯，则输出“没有举办世界杯”
 */
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.util.Scanner;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
public class IOHome_02 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		try {
		/*BufferedWriter bw=new BufferedWriter(new FileWriter("N\\worldcup.txt",true));
		bw.write("1998/法国");
		bw.newLine();
		bw.write("2002/巴西");
		bw.newLine();
		bw.write("2006/意大利");
		bw.newLine();
		bw.write("2010/西班牙");
		bw.newLine();
		bw.write("2014/德国");
		bw.newLine();
		bw.flush();
		bw.close();*/
		BufferedReader br=new BufferedReader(new FileReader("N\\worldcup.txt"));
		HashMap<String,String> list=new HashMap<String,String>();
		String[] ch=new String[2];
		String str2=new String();
		str2=br.readLine();
		while(str2!=null)
		{
			ch=str2.split("/");
			list.put(ch[0], ch[1]);
			str2=br.readLine();
		}
		br.close();
		int n=0;
		do {
			n=-1;
			System.out.print("请输入世界杯的年份：");
			String year=input.next();
			int a=0;
			for(String str:list.keySet())
			{
				if(year.equals(str))
				{
					a++;
					break;
				}
			}
			if(a==0)
			{
				System.out.println("该年份没有举办世界杯！");
			}else
			{
				System.out.println(year+"的世界杯冠军为："+list.get(year));
			}
			System.out.println("继续输入0");
			n=input.nextInt();
			
		}while(n==0);
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		

	}

}
