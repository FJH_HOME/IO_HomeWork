package IOHome;
/*
 * 2） 利用ObjectOutputStream 存入两个Staff对象， 然后使用
ObjectInputStream 读出这两个对象，并打印这两个对象的信息。
3） 写一个方法saveStaffToFile(Staff staff, File file)，该方法完成下面的
功能：
假设有一个Staff对象w1，File 对象f1 如下：
Staff staff = new Staff("Tom", 30, 5000);
File file = new File(“test.txt”);
则调用saveStaffToFile(staff, file)，会在test.txt 中增加一行：
Tom/30/5000
4） 写一个方法List<Staff> readStaffFromFile(File file)，该方法读某
个文件，从文件信息中创建一个Staff 类型的List。
例如，假设文件内容如下：
Tom/30/5000
Jim/25/3000
Terry/33/4500
则返回一个包含三个Staff 对象的List。
 */
import java.io.*;
import java.util.*;
public class Staff_Test {
	
	public static List<Staff> readStaffFromFile(File file)
	{
		List<Staff> list=new ArrayList<Staff>();
		try {
		BufferedReader br=new BufferedReader(new FileReader(file));
		String str=new String();
		str=br.readLine();
		while(str!=null)
		{
			String s[]=new String[3];
			s=str.split("/");
			int a=Integer.valueOf(s[1]);
			double b=Double.valueOf(s[2]);
			list.add(new Staff(s[0],a,b));
			str=br.readLine();
		}
		return list;
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static void saveStaffToFile(Staff staff, File file)
	{
		try {
		if(file.exists())
		{
			if(!file.isDirectory())
			{
				String N=staff.getAge()+"/"+staff.getSalary();
				BufferedWriter bw=new BufferedWriter(new FileWriter(file,true));
				bw.write(staff.getName());
				bw.write("/");
				bw.write(N);
				bw.newLine();
				bw.flush();
				bw.close();
				
			}else
			{
				System.out.println("您输入的是目录！");
			}
		}else
		{
			if(!file.getParentFile().exists())
			{
				file.mkdirs();
				saveStaffToFile(staff,file);
			}
			System.out.println("文件或目录不存在！！");
		}
      }catch(IOException e)
		{
    	  e.printStackTrace();
		}
	}

	public static void main(String[] args) {
 
		ArrayList<Staff> list=new ArrayList<Staff>();
		
		try {
			File file=new File("E:\\1.txt");
			if(!file.exists())
			{
				file.createNewFile();
			}
			ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(file));
			Staff s1=new Staff("哈哈",18,3500);
			Staff s2=new Staff("哈哈2",19,6500);
			list.add(s1);
			list.add(s2);
			oos.writeObject(list);
			oos.close();
			ObjectInputStream ois=new ObjectInputStream(new FileInputStream(file));
			ArrayList<Staff> list2=(ArrayList<Staff>)ois.readObject();
			for(Staff s:list2)
			{
				s.showPrint();
			}	
			ois.close();
			File file2=new File("E:\\2.txt");
			saveStaffToFile(s1,file2);
			saveStaffToFile(s2,file2);
			
			File file3=new File("E:\\3.txt");
			List<Staff> list3=readStaffFromFile(file3);
			for(Staff s:list3)
			{
				s.showPrint();
			}
			
	    } catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();		
		}catch(IOException e)
		{
			e.printStackTrace();
		}
       

	}

}
