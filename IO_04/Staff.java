package IOHome;
/*
 * 码云作业四：

使用ObjectInputStream和ObjectOutputStream恢复与保存员工对象。
有Staff对象，部分代码如下：
class Staff {
private String name;
private int age
private double salary;
//构造方法
…
//get/set 方法
…
//toString 方法
…
}
1） 完善Staff对象，并使其能使用对象序列化机制。

 */

public class Staff implements java.io.Serializable{
	
	private String name;
	private int age;
	private double salary;
	
	public Staff() {
		
	}

	public Staff(String name, int age, double salary) {
		super();
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public void showPrint()
	{
		System.out.println(this.getAge()+"岁的"+this.getName()+"工资为："+this.getSalary());
	}
	
	

}
